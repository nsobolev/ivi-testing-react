import 'styled-components'

import { TFonts, TColors, TDevice, TMaxDevice } from '../styled/variables'
import { ThemeModes } from '../styled/theme'

declare module 'styled-components' {
  export interface DefaultTheme {
    fonts: TFonts,
    colors: TColors,
    device: TDevice
    maxDevice: TMaxDevice,
    mode: ThemeModes,
  }
}
