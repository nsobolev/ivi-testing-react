export type TSizes = {
  phone: number,
  tablet: number,
  'tablet-landscape': number,
  desktop: number,
  'desktop-medium': number,
  'desktop-large': number,
}

type TToSizes = {
  tablet: number,
  'tablet-landscape': number,
  desktop: number,
  'desktop-medium': number,
  'desktop-large': number,
}

const sizes: TSizes = {
  'phone': 320,
  'tablet': 600,
  'tablet-landscape': 900,
  'desktop': 1240,
  'desktop-medium': 1490,
  'desktop-large': 1600,
}

const toSizes: TToSizes = {
  'tablet': 599,
  'tablet-landscape': 899,
  'desktop': 1239,
  'desktop-medium': 1489,
  'desktop-large': 1599,
}

export type TDevice = Record<keyof TSizes, string>
const keysSizes = Object.keys(sizes) as (keyof TSizes)[]

export const device = keysSizes.reduce<TDevice>((acc, size) => {
  acc[size] = `(min-width: ${sizes[size]}px)`
  return acc
}, {} as TDevice)

export type TMaxDevice = Record<keyof TToSizes, string>
const keysToSizes = Object.keys(toSizes) as (keyof TToSizes)[]

export const maxDevice = keysToSizes.reduce<TMaxDevice>((acc, size) => {
  acc[size] = `(max-width: ${toSizes[size]}px)`
  return acc
}, {} as TMaxDevice)

export type TFonts = {
  main: string;
  secondary: string;
}

export const fonts: TFonts = {
  main: '"Cera-Pro", sans-serif',
  secondary: 'Secondary',
}

export type TColors = {
  main: {
    default: string;
    light: string;
  },
  text: {
    default: string;
    contrast: string;
  },
}

type TColorsThemeApp = {
  default: TColors,
}

export const colors: TColorsThemeApp = {
  default: {
    main: {
      default: '#ea0042',
      light: '#ff0f52',
    },
    text: {
      default: '#a5a1b2',
      contrast: '#ffffff',
    },
  }
}

