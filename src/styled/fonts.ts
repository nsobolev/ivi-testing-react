import { css } from 'styled-components'
import { fontFace } from './mixins'

export const includeFonts = css`
  ${fontFace('CeraPro-Bold', 'Cera-Pro', 700)};
  ${fontFace('CeraPro-Regular', 'Cera-Pro', 400)};
  ${fontFace('CeraPro-Medium', 'Cera-Pro', 500)};
`
