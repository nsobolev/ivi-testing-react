import { createGlobalStyle } from 'styled-components'
import { includeFonts } from './fonts'

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    font-family: ${props => props.theme.fonts.main};
    font-weight: 400;
    color: ${props => props.theme.colors.text.default};
    background-color: #171133;
  }
  
  ${includeFonts};
`
