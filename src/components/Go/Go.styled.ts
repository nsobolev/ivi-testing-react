import styled, { css } from 'styled-components'

interface IRootProps {
  isHoverEmblem: boolean
}

export const Root = styled.div<IRootProps>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  background-color: #100e19;
  
  ${props => props.isHoverEmblem && css`
    background-color: #7e798f;
  `}
`

export const Emblem = styled.div`
  width: 50vw;
  height: 40vw;
  font-size: 25vw;
  color: ${props => props.theme.colors.text.contrast};
  text-align: center;
  clip-path: polygon(25% 0, 75% 0, 100% 35%, 50% 100%, 0 35%);
  background-color: ${props => props.theme.colors.main.default};
  transition: transform 0.2s ease-in-out, background-color 0.2s ease-in-out, width 0.2s ease-in-out, height 0.2s ease-in-out; 
  cursor: pointer;
  
  &:hover {
    transform: scale(1.1);
    background-color: ${props => props.theme.colors.main.light};
  }
  
  @media ${props => props.theme.device['tablet-landscape']} {
    width: 20vw;
    height: 15vw;
    font-size: 10vw;
  }
  
  @media ${props => props.theme.device.desktop} {
    width: 15vw;
    height: 12vw;
    font-size: 8vw;
  }
`
