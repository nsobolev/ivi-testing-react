import React, { useState, useEffect } from 'react'

import * as Styles from './Go.styled'

interface IGoInitialState {
  isStart: boolean
  step: number
}

interface IGoProps {
  maxCount: number
  time: number
  restartTime: number
}

const goInitialState: IGoInitialState = {
  isStart: false,
  step: 0,
}

const increment = (state: IGoInitialState) => ({
  ...state,
  step: state.step + 1
})

const reset = (state: IGoInitialState) => ({
  ...state,
  step: 0
})

const Go: React.FC<IGoProps> = ({ maxCount, time, restartTime }) => {
  const [goState, setGoState] = useState<IGoInitialState>(goInitialState)
  const [timeState, setTimeState] = useState(time)
  const [isHoverEmblem, setIsHoverEmblem] = useState(false)

  const isGameStart = () => {
    return goState.isStart
  }

  useEffect(() => {
    let int

    if (isGameStart()) {
      int = setInterval(() => {
        setGoState(prevGoState => {
          if (prevGoState.step >= maxCount) {
            return reset(prevGoState)
          }

          return increment(prevGoState)
        })
      }, timeState)
    }

    return () => {
      clearInterval(int)
    }
  }, [goState.isStart, timeState])

  useEffect(() => {
    if (goState.step === 0) {
      setTimeState(_=> restartTime)
    } else {
      setTimeState(_ => time)
    }
  }, [goState.step])



  const onClickEmblemStart = () => {
    if (!isGameStart()) {
      setGoState(prev => ({
        step: prev.step + 1,
        isStart: true,
      }))
    } else {
      setGoState(_ => ({
        ...goInitialState
      }))
    }
  }

  const onMouseEnterEmblem = () => {
    if (!isGameStart()) return

    setIsHoverEmblem(_ => true)
  }

  const onMouseLeaveEmblem = () => {
    setIsHoverEmblem(_ => false)
  }

  return <Styles.Root isHoverEmblem={ isHoverEmblem }>
    <Styles.Emblem
      onClick={ onClickEmblemStart }
      onMouseEnter={ onMouseEnterEmblem }
      onMouseLeave={ onMouseLeaveEmblem }
    >
      { getContentOfStep(goState.step) }
    </Styles.Emblem>
  </Styles.Root>
}


const getContentOfStep = (step: number): string => {
  if (step === 0) return 'Go!'
  return String(step)
}


export default Go
