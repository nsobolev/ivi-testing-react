import React from 'react'

import { ThemeProvider } from 'styled-components'
import { getThemeApp, ThemeModes } from '../../styled/theme'
import { GlobalStyle } from '../../styled/globalStyle'

import Go from '../Go'

import * as Styles from './App.styled'

const App = () => {
  const themeApp = getThemeApp(ThemeModes.Default)

  return <ThemeProvider theme={ themeApp }>
    <Styles.Root>
      <GlobalStyle />
      <Go
        time={ 1000 }
        restartTime={ 2000 }
        maxCount={3}
      />
    </Styles.Root>
  </ThemeProvider>
}

export default App
